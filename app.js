const express = require('express');
const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const promisify = require('es6-promisify');
const flash = require('connect-flash');
const expressValidator = require('express-validator');
const routes = require('./routes/index');
const helpers = require('./helpers');
const errorHandlers = require('./handlers/errorHandlers');
const BootBot = require('bootbot');
const dialogflowModule = require('./bot_modules/dialogflow');
const database = require('./database');
const timber = require('timber');

// initialize members bots
let bots = [];

// initialize timber for application logs
const transport = new timber.transports.HTTPS(process.env.TIMBER_API_KEY);
timber.install(transport);

// TODO: account for pagination

function activateNewBots() {
  database(process.env.AIRTABLE_TABLE).select({view: process.env.AIRTABLE_VIEW}).all().then(records => {
    const newBots = records.reduce((records, record) => {
      if (record.fields && record.fields.ready) {
        if (!bots[record.fields.page_id]) {
          try {
            records[record.fields.page_id] = new BootBot({
              accessToken: record.fields.page_access_token,
              verifyToken: process.env.FACEBOOK_VERIFY_TOKEN,
              appSecret: process.env.FACEBOOK_APP_SECRET,
            });
            console.info('Bot activated', {
              event: {
                bot_activated: { member: record.fields }
              }
            });

            records[record.fields.page_id].module(dialogflowModule);
          } catch (error) {
            console.error('Bot activation failed', {
              event: {
                payment_rejected: { customer_id: "abcd1234", amount: 100, reason: "Card expired" }
              }
            });
          }
        }
      } else if (bots[record.fields.page_id] && !record.fields.ready){
        console.info('Bot deactivated', {
          event: {
            bot_deactivated: { member: record.fields }
          }
        });
        delete bots[record.fields.page_id];
      }

      return records;
    }, {});

    bots = Object.assign(bots, newBots);
  }).catch((error) => {
    console.error('Airtable Failure', {
      event: {
        airtable_failure: error
      }
    });
  });
}

activateNewBots();
setInterval(activateNewBots, 10000);

// create our Express app
const app = express();

app.use(timber.middlewares.express());

// view engine setup
app.set('views', path.join(__dirname, 'views')); // this is the folder where we keep our pug files
app.set('view engine', 'pug'); // we use the engine pug, mustache or EJS work great too

// serves up static files from the public folder. Anything in public/ will just be served up as the file it is
app.use(express.static(path.join(__dirname, 'public')));

// Takes the raw requests and turns them into usable properties on req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Exposes a bunch of methods for validating data. Used heavily on userController.validateRegister
app.use(expressValidator());

// populates req.cookies with any cookies that came along with the request
app.use(cookieParser());

// Sessions allow us to store data on visitors from request to request
// This keeps users logged in and allows us to send flash messages
app.use(session({
  secret: process.env.SESSION_SECRET,
  key: process.env.KEY,
  resave: false,
  saveUninitialized: false,
  store: new MongoStore({ mongooseConnection: mongoose.connection })
}));

// // Passport JS is what we use to handle our logins
app.use(passport.initialize());
app.use(passport.session());

// // The flash middleware let's us use req.flash('error', 'Shit!'), which will then pass that message to the next page the user requests
app.use(flash());

// pass variables to our templates + all requests
app.use((req, res, next) => {
  res.locals.h = helpers;
  res.locals.flashes = req.flash();
  res.locals.user = req.user || null;
  res.locals.currentPath = req.path;
  next();
});

// promisify some callback based APIs
app.use((req, res, next) => {
  req.login = promisify(req.login, req);
  next();
});


function _verifyRequestSignature(req, res, buf) {
  var signature = req.headers['x-hub-signature'];
  if (!signature) {
    throw new Error('Couldn\'t validate the request signature.');
  } else {
    var elements = signature.split('=');
    var method = elements[0];
    var signatureHash = elements[1];
    var expectedHash = crypto.createHmac('sha1', this.appSecret)
      .update(buf)
      .digest('hex');

    if (signatureHash != process.env.FACEBOOK_VERIFY_TOKEN) {
      throw new Error('Couldn\'t validate the request signature.');
    }
  }
}

app.get('/webhook', (req, res) => {
  if (req.query['hub.mode'] === 'subscribe' &&
    req.query['hub.verify_token'] === process.env.FACEBOOK_VERIFY_TOKEN) {
    res.status(200).send(req.query['hub.challenge']);
    console.info('Successful webhook validation', {
      event: {
        successful_webhook: req.body
      }
    });
  } else {
    res.sendStatus(403);
    console.error('Failed facebook webhook validation.', {
      event: {
        failed_webhook: req.body
      }
    });
  }
});

app.post('/webhook', (req, res) => {
  const bot = bots[req.body.entry[0].id];
  if (bot) {
    bot.handleFacebookData(req.body);
  } else {
    console.error('Unknown message destination', {
      event: {
        unknown_message_destination: req.body.entry
      }
    });
  }

  res.sendStatus(200);
});

// After allllll that above middleware, we finally handle our own routes!
app.use('/', routes);

// // If that above routes didnt work, we 404 them and forward to error handler
// app.use(errorHandlers.notFound);

// One of our error handlers will see if these errors are just validation errors
app.use(errorHandlers.flashValidationErrors);

// Otherwise this was a really bad error we didn't expect! Shoot eh
if (app.get('env') === 'development') {
  /* Development Error Handler - Prints stack trace */
  app.use(errorHandlers.developmentErrors);
}

// production error handler
app.use(errorHandlers.productionErrors);

// done! we export it so we can start the site in start.js
module.exports = app;
