'use strict';
const dialogflow = require('apiai');
const app = dialogflow(process.env.DIALOGFLOW_KEY, {requestSource: "fb"});
const database = require('../database');

const chatOptions = {
  typing: true,
};

const say = ({ answer, chat }) => {
  return chat.say(answer, chatOptions);
};

async function parseTemplateVariables({ text, chat }) {
  function failProof(value) {
    return value || 'FAILED_TO_LOAD';
  }

  let lead;
  let member;
  const leadTemplateVariables = /%\(lead\./;
  const memberTemplateVariables = /%\(member\./;
  const templateVariables = /%\((\w*)\.(\w*)\)%/g;

  if (leadTemplateVariables.test(text)) {
    lead = await chat.getUserProfile();
  }

  if (memberTemplateVariables.test(text)) {
    member = await database(process.env.AIRTABLE_TABLE).select({ filterByFormula: `{page_access_token} = "${chat.bot.accessToken}"` })
      .all().then((record) => {
        return record[0].fields;
      }).catch((error) => {
        console.error('Airtable Failure', {
          event: {
            airtable_failure: { error }
          }
        });
        return;
      });
  }

  const parsedText = text.replace(
    templateVariables,
    (match, target, property) => {
      if (target === 'lead') {
        return failProof(lead[property]);
      } else if (target === 'member') {
        return failProof(member[property]);
      } else {
        return failProof(match);
      }
    }
  );

  const unsafeText = /%\(|\)%|\(%|%\)|FAILED_TO_LOAD/;

  if (unsafeText.test(parsedText)) {
    console.error('Blocked unsafe message', {
      event: {
        blocked_unsafe_message: { message: text }
      }
    });
    return;
  }

  return parsedText;
}

module.exports = (bot) => {
  bot.on('message', (payload, chat, data) => {
    chat.sendAction('mark_seen');

    const text = payload.message.text;

    const dialogflowOptions = {
      sessionId: payload.recipient.id + payload.sender.id,
    };

    var request = app.textRequest(text, dialogflowOptions);

    async function respond(response) {
      let messages = response.result.fulfillment.messages.filter((message) => {
        return message.platform === 'facebook'; }
      );

      if (messages.length === 0) {
        messages = response.result.fulfillment.messages;
      }

      console.info('Sending messages', {
        event: {
          sending_messages: {
            messages: messages,
            to: payload.recipient,
            from: { pageId: chat.pageId },
          }
        }
      });

      for (const message of messages) {
        async function textReply(message) {
          const text = await parseTemplateVariables({ text: message.speech, chat });

          await say({ answer: text, chat });

          console.info('Sent text', {
            event: {
              sent_text: {
                message: { text },
                to: payload.recipient,
                from: { pageId: chat.pageId },
              }
            }
          });
        }

        async function cardReply(message) {
          const title = await parseTemplateVariables({ text: message.title, chat });
          const subtitle = await parseTemplateVariables({ text: message.subtitle, chat });
          const imageUrl = await parseTemplateVariables({ text: message.imageUrl, chat });


          let buttons = await Promise.all(message.buttons.map(async (button) => {
            const buttonUrl = await parseTemplateVariables({text: button.postback, chat });
            const buttonTitle = await parseTemplateVariables({text: button.text, chat });
            return {
              type: "web_url",
              url: buttonUrl,
              title: buttonTitle,
            }
          }));

          const answer = [{
              title,
              image_url: imageUrl,
              subtitle,
              buttons,
            }
          ];

          await chat.sendGenericTemplate(answer);

          console.info('Sent card', {
            event: {
              sent_card: {
                message: answer,
                to: payload.recipient,
                from: { pageId: chat.pageId },
              }
            }
          });
        }

        async function quickReply(message) {
          const text = await parseTemplateVariables({ text: message.title, chat });
          const quickReplies = await Promise.all(message.replies.map(async (reply) => {
             const parsedReply = await parseTemplateVariables({ text: reply, chat });
             return parsedReply;
          }));

          const answer = { text, quickReplies };

          await say({ answer, chat });

          console.info('Sent quick reply', {
            event: {
              sent_quick_reply: {
                message: answer,
                to: payload.recipient,
                from: { pageId: chat.pageId },
              }
            }
          });
        }

        async function imageReply(message) {
          let { imageUrl: url } = message;
          url = await parseTemplateVariables({ text: url, chat });

          const answer = {
              attachment: 'image',
              url,
          };

          await say({ answer, chat, });

          console.info('Sent image', {
            event: {
              sent_image: {
                message: answer,
                to: payload.recipient,
                from: { pageId: chat.pageId },
              }
            }
          });
        }

        const chatAction = {
          0: textReply,
          1: cardReply,
          2: quickReply,
          3: imageReply,
        };

        try {
          await chatAction[message.type](message);
        } catch (error) {
          console.error('Failed to send message', {
            event: {
              failed_to_send_message: { message, error }
            }
          });
        }
      };


      // XXX: Pre-work on sending events on action
      // let eventTrigger;
      // const action = response.result.action;
      // if (action && action.startsWith('server-')) {
      //   eventTrigger = app.eventRequest({ name: action }, dialogflowOptions);

      //   eventTrigger.on('response', function(response) {
      //     console.log(response);
      //     respond(response);
      //   });

      //   eventTrigger.on('error', function(error) {
      //     console.log(error);
      //   });

      //   eventTrigger.end();
      // }
    }

    request.on('response', async function(response) {
      await respond(response);
    });

    request.on('error', function(error) {
      console.error('Facebook failure', {
        event: {
          facebook_failure: { error }
        }
      });
    });

    request.end();

    if (data.captured) { return; }
  });
};
